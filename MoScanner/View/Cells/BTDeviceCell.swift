//
//  BTDeviceCell.swift
//  MoScanner
//
//  Created by Paxi on 6/28/18.
//  Copyright © 2018 Molekule. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth

protocol BTDeviceCellDelegate {
    func didAskToConnect(cell:UITableViewCell)
    func didAskToDisconnect(cell:UITableViewCell)
}

class BTDeviceCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var statusLabel: UILabel?
    @IBOutlet weak var connectButton: UIButton?
    @IBOutlet weak var loader: UIActivityIndicatorView?
    var delegate:BTDeviceCellDelegate?
    
    @IBAction func connect() {
        if (loader?.isAnimating)! {
            loader?.stopAnimating()
            self.delegate?.didAskToDisconnect(cell: self)
        } else {
            loader?.startAnimating()
            self.delegate?.didAskToConnect(cell: self)
        }
    }
    
    func populate(peripheral: NSObject) {
        let castedPeri: MOPeripheral = peripheral as! MOPeripheral
        
        if castedPeri.peripheral?.state == CBPeripheralState.connecting &&
            !(self.loader?.isAnimating)! {
            self.loader?.startAnimating()
        } else if castedPeri.peripheral?.state != CBPeripheralState.connecting &&
        (self.loader?.isAnimating)! {
            self.loader?.stopAnimating()
        }
        
        if (castedPeri.peripheral?.name != nil &&
            (castedPeri.peripheral?.name?.count)! > 0) {
            self.titleLabel?.text = castedPeri.peripheral?.name
        } else {
            self.titleLabel?.text = "No name"
        }
        
        if (castedPeri.adittionalData!["kCBAdvDataIsConnectable"] != nil &&
            castedPeri.adittionalData!["kCBAdvDataIsConnectable"] as! Bool == true) {
            self.statusLabel?.text = "Connectable"
            self.connectButton?.isHidden = false
        } else {
            self.statusLabel?.text = "Not Connectable"
            self.connectButton?.isHidden = true
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.connectButton?.layer.cornerRadius = 4
        self.connectButton?.layer.borderColor = self.connectButton?.tintColor.cgColor
        self.connectButton?.layer.borderWidth = 1.0
    }
}
