//
//  ViewController.swift
//  MoScanner
//
//  Created by Paxi on 6/27/18.
//  Copyright © 2018 Molekule. All rights reserved.
//

import UIKit

class ScannerDashboard: UIViewController, BluetoothManagerDelegate, UITableViewDelegate, UITableViewDataSource, BTDeviceCellDelegate, DetailsScreenDelegate {
    
    //MARK properties
    @IBOutlet weak var scanButton : UIButton!
    @IBOutlet weak var deviceTable : UITableView!
    @IBOutlet weak var noDeviceMessage : UILabel!
    let scanner = BluetoothManager.init()

    //MARK Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.deviceTable.register(UINib(nibName: "BTDeviceCell", bundle: nil), forCellReuseIdentifier: "btdevice")
        
        self.giveButtonStyle()
        scanner.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK Table
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let devices = self.scanner.peripherals.count
        
        self.deviceTable.isHidden = devices == 0
        self.noDeviceMessage.isHidden = devices != 0
        
        return devices
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell? = nil
        
        cell = tableView.dequeueReusableCell(withIdentifier: "btdevice")
        
        let castedCell: BTDeviceCell = cell as! BTDeviceCell
        
        castedCell.populate(peripheral: self.scanner.peripherals[indexPath.row])
        castedCell.delegate = self;
        
        return cell!
    }
    
    //MARK Actions
    
    @IBAction func scanOrStop() {
    
        if (self.scanner.isOn()) {
            if (self.scanner.centralManager?.isScanning)! {
                self.scanner.centralManager?.stopScan()
                self.scanButton.setTitle("Scan", for: UIControlState.normal)
                self.scanButton.setTitle("Scan", for: UIControlState.selected)
            } else {
                self.scanner.resetScan()
                self.scanButton.setTitle("Stop", for: UIControlState.normal)
                self.scanButton.setTitle("Stop", for: UIControlState.selected)
                self.deviceTable.reloadData()
            }
        } else {
            let alert:UIAlertController = UIAlertController.init(title: nil, message: "Please turn on Bluetooth in your Settings or enable permissions for this app.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler:{ (alert2: UIAlertAction!) in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK BTDeviceCellDelegate
    func didAskToConnect(cell: UITableViewCell) {
        if (self.scanner.centralManager?.isScanning)! {
            self.scanOrStop()
        }
        
        let index = self.deviceTable.indexPath(for: cell)?.row
        self.scanner.connectTo(index: index!)
        
        self.deviceTable.reloadData()
    }
    
    func didAskToDisconnect(cell: UITableViewCell) {
        if (self.scanner.centralManager?.isScanning)! {
            self.scanOrStop()
        }
        self.deviceTable.reloadData()
    }
    
    //MARK DetailScreenDelegate
    
    func didFinishConnection(peri: MOPeripheral) {
        self.scanner.disconnectSafely(peri:peri)
        self.deviceTable.reloadData()
    }
    
    //MARK BTDelegate
    func didConnect (index : NSInteger) {
        let detail:DetailScreen = DetailScreen.init()
        detail.peripheral = scanner.peripherals[index]
        detail.delegate = self
        self.addChildViewController(detail)
        self.view.addSubview(detail.view)
    }
    
    func didUpdatePeripherals() {
        self.deviceTable.reloadData()
    }
    
    func didTurnOff() {
        if (self.scanner.centralManager?.isScanning)! {
            let alert:UIAlertController = UIAlertController.init(title: nil, message: "Please turn on Bluetooth again to continue scanning.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction.init(title: "Ok", style: UIAlertActionStyle.default, handler:{ (alert2: UIAlertAction!) in
                alert.dismiss(animated: true, completion: nil)
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func didTurnOn() {
        //self.scanOrStop();
    }
    
    //MARK Utilities
    func giveButtonStyle() {
        self.scanButton.layer.cornerRadius = 4
        self.scanButton.layer.borderColor = (UIColor.darkGray.cgColor as CGColor)
        self.scanButton.layer.borderWidth = 1.0
    }
}

