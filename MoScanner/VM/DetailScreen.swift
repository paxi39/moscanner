//
//  DetailScreen.swift
//  MoScanner
//
//  Created by Paxi on 6/29/18.
//  Copyright © 2018 Molekule. All rights reserved.
//

import Foundation
import UIKit
import CoreBluetooth

protocol DetailsScreenDelegate {
    func didFinishConnection(peri:MOPeripheral)
}

class DetailScreen: UIViewController, UITableViewDelegate, UITableViewDataSource, CBPeripheralDelegate {
    
    //MARK Properties
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var uuidValue: UILabel!
    @IBOutlet weak var manuValue: UILabel!
    @IBOutlet weak var charTable: UITableView!
    
    var delegate : DetailsScreenDelegate? = nil
    var peripheral :MOPeripheral!
    
    //MARK Table methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return peripheral.peripheral?.services == nil ? 0 : (peripheral.peripheral?.services?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "attribute")
        
        if cell == nil {
            cell = UITableViewCell.init(style: UITableViewCellStyle.default, reuseIdentifier: "attribute")
        }
        
        var service : CBService = (peripheral.peripheral?.services?[indexPath.row])!
        
        cell?.textLabel?.text = "Service UUID:\(service.uuid.uuidString)"
        
        return cell!
    }
    
    //MARK Actions
    @IBAction func dismiss() {
        self.delegate?.didFinishConnection(peri: self.peripheral)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    //MARK Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.view.layer.cornerRadius = 4.0
        self.view.layer.borderColor = UIColor.darkGray.cgColor
        self.view.layer.borderWidth = 1.0
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.uuidValue.text = self.peripheral.peripheral?.identifier.uuidString
        self.titleName.text = self.peripheral.peripheral?.name
        
        if self.peripheral.adittionalData!["kCBAdvDataManufacturerData"] != nil {
            self.manuValue.text = MOPeripheral.parseManufacturerInfo(rawData: self.peripheral.adittionalData!["kCBAdvDataManufacturerData"] as! Data)
        } else {
            self.manuValue.text = "Not Available"
        }
        self.peripheral.peripheral?.delegate = self
        self.peripheral.peripheral?.discoverServices(nil)
    }
    
    //MARK Peripheral
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        self.charTable.reloadData()
        
        //TODO discover characteristics
        //peripheral.discoverCharacteristics(nil, for: (peripheral.services?[0])!)
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        //TODO filters useful characteristics and refresh table
    }
}
