//
//  MOPeripheral.swift
//  MoScanner
//
//  Created by Paxi on 6/29/18.
//  Copyright © 2018 Molekule. All rights reserved.
//

import Foundation
import CoreBluetooth

class MOPeripheral: NSObject {
    var peripheral : CBPeripheral?
    var adittionalData: [String : Any]?
    
    override init() {
        super.init()
    }
    
    class func parseManufacturerInfo(rawData: Data) -> String {
        
        let manufacturerId:Range<Int> = 0..<2
        let nodeId:Range<Int> = 2..<4
        let nodeState:Range<Int> = 4..<5
        let voltage:Range<Int> = 5..<6
        let packetCounter:Range<Int> = 6..<9
        
        let subdata1 = manufacturerId.upperBound <= rawData.count ? rawData.subdata(in: manufacturerId) : nil
        let subdata2 = nodeId.upperBound <= rawData.count ? rawData.subdata(in: nodeId) : nil
        let subdata3 = nodeState.upperBound <= rawData.count ? rawData.subdata(in: nodeState) : nil
        let subdata4 = voltage.upperBound <= rawData.count ? rawData.subdata(in: voltage) : nil
        let subdata5 = packetCounter.upperBound <= rawData.count ? rawData.subdata(in: packetCounter) : nil
        
        //Results from original given string
        let str1 = subdata1 != nil ? String(data: subdata1!, encoding:.utf8) : ""
        let str2 = subdata2 != nil ? String(data: subdata2!, encoding:.utf8) : ""
        let str3 = subdata3 != nil ? String(data: subdata3!, encoding:.utf8) : ""
        let str4 = subdata4 != nil ? String(data: subdata4!, encoding:.utf8) : ""
        let str5 = subdata5 != nil ? String(data: subdata5!, encoding:.utf8) : ""
        
        return "ID:\(str1 ?? "None")\nNode ID:\(str2 ?? "None")\nNode State:\(str3 ?? "None")\nBattery V:\(str4 ?? "None")\nPacket Counter:\(str5 ?? "None")"
    }
}
