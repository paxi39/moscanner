
//
//  BluetoothManager.swift
//  MoScanner
//
//  Created by Paxi on 6/27/18.
//  Copyright © 2018 Molekule. All rights reserved.
//

import Foundation
import CoreBluetooth

protocol BluetoothManagerDelegate{
    func didTurnOn()
    func didTurnOff()
    func didUpdatePeripherals()
    func didConnect(index : NSInteger)
}

class BluetoothManager: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    var centralManager: CBCentralManager?
    var delegate: BluetoothManagerDelegate?
    var peripherals = Array<MOPeripheral>()
    
    func centralManagerDidUpdateState(_ manager: CBCentralManager) {
        if (manager.state == CBManagerState.poweredOn) {
            self.delegate?.didTurnOn()
        } else {
            self.delegate?.didTurnOff()
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        var present:Bool = false
        for peri in peripherals {
            if peri.peripheral?.identifier.uuidString == peripheral.identifier.uuidString {
                present=true
            }
        }
        
        if !present {
            let newPeri = MOPeripheral.init()
            newPeri.peripheral = peripheral
            peripheral.delegate = self
            newPeri.adittionalData = advertisementData
            peripherals.append(newPeri)
            self.delegate?.didUpdatePeripherals()
        }
    }
    
    func centralManager(_ central: CBCentralManager,
                                 didConnect peripheral: CBPeripheral) {
        
        var index = 0
        
        for peri in self.peripherals {
            if peri.peripheral?.identifier.uuidString == peripheral.identifier.uuidString {
                break;
            }
            index+=1
        }
        
        self.delegate?.didConnect(index:index)
    }
    
    func centralManager(_ central: CBCentralManager,
                        didDisconnectPeripheral peripheral: CBPeripheral,
                        error: Error?) {
        
    }
    
    func centralManager(_ central: CBCentralManager,
                        didFailToConnect peripheral: CBPeripheral,
                        error: Error?) {
        
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        
    }
    
    func isOn() -> Bool {
        return centralManager?.state == CBManagerState.poweredOn
    }
    
    func resetScan() {
        peripherals.removeAll()
        centralManager?.scanForPeripherals(withServices: nil, options: nil)
    }
    
    func disconnectSafely (peri: MOPeripheral) {
        if peri.peripheral?.state == CBPeripheralState.connected {
            self.centralManager?.cancelPeripheralConnection(peri.peripheral!)
        }
    }
    
    func connectTo(index : NSInteger) {
        
        for peri in peripherals {
            if peri.peripheral?.state == CBPeripheralState.connected ||
               peri.peripheral?.state == CBPeripheralState.connecting {
                centralManager?.cancelPeripheralConnection(peri.peripheral!)
            }
        }
        
        centralManager?.connect(peripherals[index].peripheral!, options: nil)
    }

    override init() {
        super.init()
        self.centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main, options:nil)
    }
}
