# MoScanner


Known issues:

- Missing timeout implementation for connection attempt.
- Missing characteristics fetch and proper parsing. (There were multiple considerations to take here, we can discuss them in the interview.)
- Missing indicator (more UX friendly) when scanning for devices.
- Missing error handling.
- Missing clear disconnection flow.



Clarifications:

- Navigation wasn't implemented traditionally (with a navigation controller and a nav bar) because it was a very plain app, so I prefered to use simple views to have more flexibility in size and style of buttons. Definitely wouldn't handle it that way in a more complex or final version of an app, but for a test I guess is ok. In other cases I would even think of subclassing the nav bar, but no need for just 1 screen.
- RxSwift not used: Mainly because I don't think the use of RxSwift is necessary in this example (I honestly think very little ocassions require such implementation, I think of it more of a trendy way of doing things). Anyhow I know it was required and didn't implement it mainly because of lack of time, it may have been useful for making the detail screen an observer of the characteristics and services of the peripherals to have a more comfortable way to refresh it.
- No test cases: Again not much time, and didn't identify an area where it would be that strongly required. I'd implemented tests in this project mainly on the parsers of the different info on the peripherals since most of them are optional and come in kind of an unformatted dictionary. In the interactions with bluetooth any test cases would require mocking objects, which I didn't had the time to do.
- I usually add Cocoapods as a first step of any project, but since this was a fast implementation and didn't require any libraries so far I didn't add it, but I sure think is a must in every project, makes things really easy.
- I haven't recently done this kind of tests because I find them to go against the real meaning of software development, but in this ocassion I hope this project somehow reflects some of my knowledge in the area. However I'd really like to clarify that I think the development of an app should never be even considered in such a short timeframe, because it incentivates the idea of development without QA, proper UX and even correct programming practices. (If all of this elements were to be taken into account, a test this size would require more effort than I'd consider correct to ask from a candidate)



Thanks for your time and for considering me a candidate for your team!
